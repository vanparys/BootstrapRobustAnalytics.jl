"""
    Smoother

    Defines a smoother function S
"""
struct Smoother
    "name"
    name
    "function"
    func
end

Base.show(io::IO, S::Smoother) = print(io, S.name)
Docs.getdoc(t::Smoother) = "Documentation of the $(t.name)"

function (S::Smoother)(d)
    return S.func(d)
end

"""
    Weigther

    Defines a weighter function Sn
"""
struct Weighter
    "Smoother function"
    smoother::Smoother
    "Distance metric"
    distance
    "Bandwith parameter"
    bandwidth
end

Base.show(io::IO, Sn::Weighter) = print(io, Sn.smoother, " with bandwidth h=", Sn.bandwidth)
Docs.getdoc(t::Weighter) = "Documentation of the $(t.smoother)"

function (Sn::Weighter)(x1, x2)
    return Sn.smoother(Sn.distance(x1, x2)/Sn.bandwidth)
end

"""
    naive_smoother_function(d)

# Arguments

    1. `d` : Distance between two points

# Returns

    1. `Sn` : Smoother coefficient
"""
function naive_smoother_function(d)

    return 1.0
end

naive_smoother = Smoother("Naive Smoother", naive_smoother_function)

"""
    uniform_smoother_function(d)

# Arguments

    1. `d` : Distance between two points

# Returns

    1. `Sn` : Smoother coefficient
"""
function uniform_smoother_function(d)

    Sn = 0.0
    if abs(d)<= 1
        Sn = 0.5
    end
    
    return Sn
end

uniform_smoother = Smoother("Uniform Smoother", uniform_smoother_function)

"""
    triangular_smoother_function(d)

# Arguments

    1. `d` : Distance between two points

# Returns

    1. `Sn` : Smoother coefficient
"""
function triangular_smoother_function(d)
    
    Sn = 0
    if abs(d)<= 1
        Sn = 1-d
    end
    
    return Sn
end

triangular_smoother = Smoother("Triangular Smoother", triangular_smoother_function)

"""
    epanechnikov_smoother_function(d)

# Arguments

    1. `d` : Distance between two points

# Returns

    1. `Sn` : Smoother coefficient
"""
function epanechnikov_smoother_function(d)

    Sn = 0
    if abs(d)<= 1
        Sn = 3/4*(1-d^2)
    end
    
    return Sn
end

epanechnikov_smoother = Smoother("Epanechnikov Smoother", epanechnikov_smoother_function)

"""
    quartic_smoother_function(d)

# Arguments

    1. `d` : Distance between two points

# Returns

    1. `Sn` : Smoother coefficient
"""
function quartic_smoother_function(d)

    Sn = 0
    if abs(d)<= 1
        Sn = 15/16*(1-d^2)^2
    end
    
    return Sn
end

quartic_smoother = Smoother("Quartic Smoother", quartic_smoother_function)

"""
    triweight_smoother_function(d)

# Arguments

    1. `d` : Distance between two points

# Returns

    1. `Sn` : Smoother coefficient
"""
function triweight_smoother_function(d)

    Sn = 0
    if abs(d)<= 1
        Sn = 35/32*(1-d^2)^3
    end
    
    return Sn
end

triweight_smoother = Smoother("Triweight Smoother", triweight_smoother_function)

"""
    tricubic_smoother_function(d)

# Arguments

    1. `d` : Distance between two points

# Returns

    1. `Sn` : Smoother coefficient
"""
function tricubic_smoother_function(d)
    
    return max(70/81*(1-d^3), 0)
end

tri_cubic_smoother = Smoother("Tricubic Smoother", tricubic_smoother_function)

"""
    gaussian_smoother_function(d)

# Arguments

    1. `d` : Distance between two points

# Returns

    1. `Sn` : Smoother coefficient
"""
function gaussian_smoother_function(d)
    
    return exp(-d^2)/sqrt(2*π)
end

gaussian_smoother = Smoother("Gaussian Smoother", gaussian_smoother_function)

"""
    cosine_smoother_function(d)

# Arguments

    1. `d` : Distance between two points

# Returns

    1. `Sn` : Smoother coefficient
"""
function cosine_smoother_function(d)

    Sn = 0
    if abs(d)<= 1
        Sn = π/4*cos(π/2*d)
    end
    
    return Sn
end

cosine_smoother = Smoother("Cosine Smoother", cosine_smoother_function)

"""
    logistic_smoother_function(d)

# Arguments

    1. `d` : Distance between two points

# Returns

    1. `Sn` : Smoother coefficient
"""
function logistic_smoother_function(d)

    return 1/(exp(d)+2+exp(-d))
end

logistic_smoother = Smoother("Logistic Smoother", logistic_smoother_function)

"""
    sigmoid_smoother_function(d)

# Arguments

    1. `d` : Distance between two points

# Returns

    1. `Sn` : Smoother coefficient
"""
function sigmoid_smoother_function(d)

    return 2/π*1/(exp(d)+exp(-d))
end

sigmoid_smoother = Smoother("Sigmoid Smoother", sigmoid_smoother_function)
