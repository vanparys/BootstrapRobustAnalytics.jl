"""
    nearest_neighbors_learner(Y, X, Xbar, d, k, Sn[, verbose] )

# Arguments

    1. `Y` : Array of observed responses
    2. `X` : Covariate data
    3. `xbar` : Context of interest
    4. `d` : Distance metric
    5. `k` : Number of considered neighbors
    6. `Sn` : Weighter function
    7. `verbose` : Verbosity

# Returns
    1. `Yp` : Nearest neighbors prediction
  
"""
function nearest_neighbors_learner(Y, X, Xbar, d, k, Sn; verbose=false)

    n = size(Y, 1)
    d_y = size(Y, 2)
    m = size(Xbar, 1)

    if verbose
        println("# Start Nearest Neighbors Learning")
        println("**********************************")
        println("# Hyper Parameters")
        println("1. Number of nearest neighbors k = ", k)
        println("2. Smoother function S = ", Sn)
        println("# Problem Parameters")
        println("1. Number of samples n = ", n)
        println("2. Label dimension : ", d_y)
        println("3. Number of contexts : ", m)
    end

    k = min(max(1, k), n)

    Yp = zeros(m, d_y)      
    for j in 1:m

        ## Context of interest
        xbar = Xbar[j, :]
        
        ## SORT the data based on distance to xbar
        dist = [d(X[i, :], xbar ) for i in 1:n]
        perm = sortperm(dist)
        dist = dist[perm]
        Y = Y[perm, :]
        X = X[perm, :]
        
        ## Define set of points of interest
        R_star = dist[k]+1e-7
        Nk = find(s->s<=R_star, dist)

        ## Weights
        S = [Sn(X[i, :], xbar) for i in Nk]
        s_star = sum(S)

        ## Prediction
        Yp[j, :] = sum([S[i]*Y[i, :] for i in Nk], 1)[1]/s_star

    end

    if verbose
        println("**************************************")
        println("*** End Nearest Neighbors Learning ***")
    end

    return Yp
end

"""
    nearest_neighbors_cv(Y, X, f[, S_all])

# Arguments
  1. `Y` : Array of observed responses
  2. `X` : Covariate data
  3. `f` : Number of folds for cross validation
  4. 'S_All=naive_smoother': smoothers considered 
  5. 'verbose' : Verbosity

# Returns

  1. `d` : Distance function
  2. `k` : Number of nearest neighbors
  3. `Sn` : Validated weighing function
"""
function nearest_neighbors_cv(Y, X, f; S_all=[naive_smoother], verbose=false)

    n = size(X, 1)
    d_x = size(X, 2)
    d_y = size(Y, 2)

    if verbose
        println("# Start Nearest Neighbors Hyper Parameter Cross Validation")
        println("**********************************************************")
        println("## Problem Parameters")
        println("1. Number of samples n = ", n)
        println("2. Label dimension : ", d_y)
        println("3. Covariate dimension : ", d_y)
        println("## Hyperparameter optimization")
        println("1. Number of folds f =", f)
        println("2. Considered Smoothers : ", S_all) 
    end

    # Compute covariance of covariates
    μ = mean(X, 1)
    Σ = X'*X/n-μ'*μ+eye(d_x)/n
    
    Σ = factorize(Σ)

    # Distance function
    d = (x1, x2)-> sqrt((collect(x1[:])-collect(x2[:]))'*(Σ\(collect(x1[:])-collect(x2[:]))))[1]
    
    # hyperparameters
    D = [d(X[i, :], μ) for i in 1:n]
    k_all = unique(round.(Int, linspace(max(1, sqrt(n)/5), min(n, sqrt(n)*5), 50)))
    k_all = 1:round(Int, n/4)
    
    # f Fold cross validation
    folds = collect(Kfold(n, f))
    
    val_errors = []
    for S in S_all, k in k_all

        h_all = logspace(log10(minimum(D)), log10(maximum(D)), 10)
        if S==naive_smoother
            h_all = [1]
        end

        for hn in h_all

            Sn = Weighter(S, d, hn)
            if verbose
                println("*****CROSS-VALIDATION*****")
                println(Sn)
                println("Number of neighbors : k = ", k)
            end

            val_error = []
            for train in folds

                val = setdiff(1:n, train)
                Yp = nearest_neighbors_learner(Y[train, :], X[train, :], X[val, :], d, k, Sn)
                val_error = push!(val_error, sqrt(sum((Y[val, :]-Yp).^2))/length(val))

            end
            push!(val_errors, [mean(val_error), Sn, k])

        end
    end

    val_errors_star = val_errors[indmin([val_errors[i][1] for i in 1:length(val_errors)])]
    
    if verbose
        println("**************************************************************")
        println("*** End Nearest Neighbors Hyper Parameter Cross Validation ***")
    end

    return d, val_errors_star[3], val_errors_star[2], [val_errors[i][1] for i in 1:length(val_errors)]
end

"""
    rel_entr_j(j, k, n)

    # Arguments

          1. 'j' : Neighborhood parameter
          2. 'k' : Neighbors parameter
          3. 'n' : Number of samples
          4. 'verbose' : Verbosity

    # Returns

          1. 'r' : Radius
          2. 's' : Solver status
    """
  function rel_entr_j(j, k, n; verbose=false)

      if verbose
          println("# Start Relative Entropy Minimization over Neighborhood Set")
          println("***********************************************************")
          println("1. Number of nearest neighbors k = ", k)
          println("2. Neighborhood parameter j = ", j)
          println("3. Number of samples n = ", n)
      end
      
      m_b = Variable(3)
      m_t = Constant([(j-1)/n, 1/n, (n-j)/n])

      ## Objective
      obj = Convex.relative_entropy(m_b, m_t)

      ## Constraints
      constr = [sum(m_b)==1, m_b[1]+m_b[2]>= k/n, m_b[1] <= (k-1)/n]
      
      problem = minimize(obj, constr)
      solve!(problem, ECOSSolver(verbose=verbose))

      if verbose
          println("***End Relative Entropy Minimization over Neighborhood Set ***")
          println("**************************************************************")
      end

      return problem.optval, problem.status
  end

"""
    bisection(f, l, u[, ϵ])
    
    Use bisection to find the root r of a function f.

    # Arguments

          1. 'f' : Monotonically decreasing function
          2. 'l' : Lower bound on root
          3. 'u' : Upper bound on root
          4. 'ϵ' : Tolerance

    # Returns

          1. 'r_est' : Root estimate
          2. ('l', 'u') : l <= r <= u with u-l<=ϵ  
"""
function bisection(f, l, u; ϵ=1e-9)

    f_u = f(u)
    f_l = f(l)

    while u-l>ϵ
        m = (u+l)/2
        f_m = f(m)

        if f_m>0
            l = m
            f_l = f_m
        else
            u = m
            f_u = f_m
        end
    end

    return (u+l)/2, l, u
end

"""
    bootstrap_radius(j, k, n[, verbose])

    # Arguments

          1. 'j' : Neighborhood parameter
          2. 'k' : Neighbors parameter
          3. 'β' : Bootstrap disappointment
          4. 'verbose' : Verbosity        

    # Returns

          1. 'r' : Radius
"""
function bootstrap_radius(k, n, β; verbose=false)

    if verbose
        println("# Start Bootstrap Radius")
        println("************************")
        println("1. Number of nearest neighbors k = ", k)
        println("2. Number of samples n = ", n)
        println("3. Disappointment b = ", β)
    end

    r_star = [rel_entr_j(j, k, n)[1] for j=1:n]
    b = r -> sum([exp(-n*max(r, r_star[j])) for j in 1:n])

    l = 1/n*log(1/β)
    u = log(n)

    if verbose
        println("********************")
        println("End Bootstrap Radius")
    end

    return bisection(r->b(r)-β, l, u)[1]
end

"""
    J_feasible(k, n, r)

    # Arguments

          1. 'k' : Neighbors parameter
          2. 'n' : Number of samples
          3. 'r' : Robustness parameter
          
    # Returns

          1. 'J' : Feasible neighborhoods
"""
function J_feasible(k, n, r)

    J_plus = []
    j = k
    r_star = 0

    while r_star <= r && j<=n
        r_star, status = rel_entr_j(j, k, n)
        if r_star <= r
            J_plus = vcat(J_plus, [j])
        end
        j = j+1
    end

    J_minus = []
    j = k
    r_star = 0
    while r_star <= r && j>=1
        r_star, status = rel_entr_j(j, k, n)
        if r_star <= r
            J_minus = vcat(J_minus, [j])
        end
        j = j-1
    end

    return sort(union(J_plus, J_minus))
end

"""
    nearest_neighbors_bootstrap_dual(L, X, d, k, Sn, xbar, r[, verbose])

# Arguments
    1. `L` : Array of observed costs
    2. `X` : Covariate data
    3. 'd' : Distance Metric
    4. 'k' : Number of neighbors
    5. `Sn` : Weighter function
    6. `xbar` : Context of interest
    7. 'r' : Robustness parameter
    8. 'verbose' : Verbosity
              
            
# Returns
    1. `c_r` : Robust cost
    2. 's' : Solver status
"""
function nearest_neighbors_bootstrap_dual(L, X, d, k, Sn, xbar, r; verbose=false)

    n = size(L, 1)
    d_y = size(L, 2)

    if verbose
        println("# Start Nearest Neighbors Dual Robust Formulation")
        println("*************************************************")
        println("## Problem dimensions")
        println("Number of Samples n = ", n)
        println("Label dimension : ", d_y)
        println("## Hyper parameters")
        println("1. Weighter Sn : ", Sn)
        println("2. Number of nearest neighbors k = ", k)
        println("2. Robustness r = ", r)
    end
    
    # SORT the data based on distance to xbar
    dist = [d(X[i, :], xbar) for i in 1:n]
    perm = sortperm(dist)
    dist = dist[perm]
    L_nn = L[perm]
    X_nn = X[perm, :]

    J = J_feasible(k, n, r)
    
    # Optimization
    α = Variable(1)
    constrs = []
    for j in J
        
        # Dual Variables
        η = Variable(2)
        η.value = zeros(2)
        ν = Variable(1)
        u = Variable(j+1)

        s_j = sum([Sn(X_nn[i, :], xbar) for i in 1:j])
        
        constrs = [u>=0, ν>=0, η>=0]
        constrs = constrs + [sum(u[1:j]) + (n-j)*u[j+1] <= n*ν*exp(-r)]
        
        for i in 1:j-1
            constrs = constrs + [(L_nn[i]-α)*Sn(X_nn[i, :], xbar)/s_j*n <= log_perspective(n*u[i], n*ν) + (k-n)/k*η[1] + (1-k+n)/k*η[2]]
        end
        constrs = constrs + [(L_nn[j]-α)*Sn(X_nn[j, :], xbar)/s_j*n  <= log_perspective(n*u[j], n*ν) + (k-n)/k*η[1] + (1-k)/k*η[2] ]
        constrs = constrs + [0<=log_perspective(n*u[j+1], n*ν) + η[1] + (1-k)/k*η[2]]
        
    end
    
    problem = minimize(α, constrs)
    solve!(problem, ECOSSolver(maxit=1000, reltol=1e-12, verbose=verbose))
    # if problem.status!=:Optimal
    #     solve!(problem, SCSSolver(max_iters=50000, reltol=1e-12), warmstart=true)
    # end

    if verbose
        println("*****************************************************")
        println("*** End Nearest Neighbors Dual Robust Formulation ***")
    end
    
    return problem.optval, problem.status
    
end

"""
    nearest_neighbors_bootstrap_primal(L, X, d, k, Sn, xbar, r[, verbose])

# Arguments
    1. `L` : Array of observed costs
    2. `X` : Covariate data
    3. 'd' : Distance Metric
    4. 'k' : Number of neighbors
    5. `Sn` : Weighter function
    6. `xbar` : Context of interest
    7. 'r' : Robustness parameter
    8. 'verbose' : Verbosity
              
            
# Returns
    1. `c_r` : Robust cost
    2. '(P^nn, s^nn, j^nn)' : Primal optimal variables 
    3. 's' : Solver status
"""
function nearest_neighbors_bootstrap_primal(L, X, d, k, Sn, xbar, r; verbose=false)

    n = size(L, 1)
    d_y = size(L, 2)

    if verbose
        println("# Start Nearest Neighbors Primal Robust Formulation")
        println("***************************************************")
        println("## Problem dimensions")
        println("Number of Samples n = ", n)
        println("Label dimension : ", d_y)
        println("## Hyper parameters")
        println("1. Weighter Sn : ", Sn)
        println("2. Number of nearest neighbors k = ", k)
        println("2. Robustness r = ", r)
    end

    # SORT the data based on distance to xbar
    dist = [d(X[i, :], xbar) for i in 1:n]
    perm = sortperm(dist)

    L_nn = L[perm]
    X_nn = X[perm, :]

    # Determine feasible neighborhood parameters
    J = J_feasible(k, n, r)

    s_nn = 0
    P_nn = zeros(n) 
    v_nn = minimum(L_nn)
    st_nn = :Optimal
    j_nn = 0
    for j in J
        #
        # Primal Variables
        s = Variable(1)
        P = Variable(j+1)
        #
        # Objective
        obj = sum([L_nn[i]*Sn(X_nn[i, :], xbar)*P[i] for i in 1:j])/n
        #
        # Constraints
        constrs = [P>=0, sum(P)==s*n, sum([Sn(X_nn[i, :], xbar)*P[i] for i in 1:j])==n]
        constrs = constrs + [sum(P[1:j])>=k*s]
        if j>1
            constrs = constrs + (sum(P[1:j-1])<=(k-1)*s)
        end
        constrs=constrs+[relative_entropy(P[1:j], s*ones(j))+relative_entropy(P[j+1], s*(n-j))<=s*r*n]
        #
        # Optimization problem
        problem = maximize(obj, constrs)
        #
        solve!(problem, ECOSSolver(maxit=1000, abstol=1e-12, verbose=verbose))
        # if problem.status!=:Optimal
        #     solve!(problem, SCSSolver(max_iters=50000, reltol=1e-12, verbose=verbose), warmstart=true)
        # end
        #
        if problem.optval > v_nn
            s_nn = s.value
            P_nn = P.value 
            v_nn = problem.optval
            st_nn = problem.status
            j_nn = j
        end
        #
    end

    if verbose
        println("*******************************************************")
        println("*** End Nearest Neighbors Primal Robust Formulation ***")
    end

    return v_nn, P_nn, s_nn, j_nn, st_nn
end

"""
    nearest_neighbors_bs(Y, X, xbar, d, k, Sn[, n, m])

# Arguments

    1. `Y` : Array of observed responses
    2. `X` : Covariate data
    3. `xbar` : Context of interest
    4. `d` : Distance metric
    5. `k` : Number of neighbors
    6. `Sn` : Weighter function
    7. `(n, m)` : Bootstrap samples / multiplicity
    8. 'verbose' : Verbosity

# Returns
    1. `Yp_boot` : NN prediction on m bootstrap resamples involving n data points
  
"""
function nearest_neighbors_bs(Y, X, xbar, d, k, Sn; n=0, m=1000, verbose=false)

    d_y = size(Y, 2)
    N = size(X, 1)

    if n==0
        n=N
    end

    if verbose
        println("# Start Nearest Neighbors Bootstrap")
        println("*********************************")
        println("1. Total number of points : ", N)
        println("2. Number of samples per bootstrap : ", n)
        println("3. Total number of bootstraps : ", m)
    end
    
    Yp_boot =zeros(m, d_y)
    for i in 1:m
        
        if verbose && (i % 1000 == 0)
            println("Bootstrap Epoch : ", i)
        end

        bs = wsample(1:N, ones(N)/N, n)
        Yp_boot[i, :] = nearest_neighbors_learner(Y[bs, :], X[bs, :], xbar, d, k, Sn)
    end

    if verbose
        println("***End Nearest Neighbors Bootstrap***")
        println("*************************************")          
    end

    return Yp_boot   
end
