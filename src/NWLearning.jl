"""
    nadaraya_watson_learner(Y, X, xbar, Sn[, verbose])

# Arguments

    1. `Y` : Array of observed responses
    2. `X` : Covariate data
    3. `xbar` : Context of interest
    4. `Sn` : Weighter function
    5. 'verbose' : Verbosity
  
# Returns
    1. `Yp` : Nadaraya-Watson prediction
  
"""
function nadaraya_watson_learner(Y, X, Xbar, Sn; verbose=false)
    n = size(Y, 1)
    d_y = size(Y, 2)
    m = size(Xbar, 1)

    if verbose
      println("# Start Nadaraya Watson Learning")
      println("********************************")
      println("# Hyper Parameters")
      println("2. Smoother function S = ", Sn)
      println("# Problem Parameters")
      println("1. Number of samples n = ", n)
      println("2. Label dimension : ", d_y)
      println("3. Number of contexts : ", m)
    end

    Yp = zeros(m, d_y)
    
    for j in 1:m
        # Context of interest
        xbar = Xbar[j, :]
        # Weights
        S = [Sn(X[i, :], xbar) for i in 1:n]
        s_star = sum(S)
        # NW Prediction
        Yp[j, :] = sum([S[i]*Y[i, :] for i in 1:n], dims=1)[1]/s_star
    end

    if verbose
        println("************************************")
        println("*** End Nadaraya Watson Learning ***")
    end

    return Yp
end

"""
    nadaraya_watson_v(Y, X[, p, S_all, verbose])

Determine best Nadaraya-Watson learner based on train/validation split.

# Arguments

  1. `Y`: Array of observed responses.
  2. `X`: Covariate data.
  3. `p=0.2`: validation-data/all-data.
  4. `S_all=gaussian_smoother`: smoothers considered.
  5. 'verbose' : Verbosity

# Returns
  1. `Sn`: validated smoother function
"""
function nadaraya_watson_v(Y, X; p=0.2, S_all=[gaussian_smoother], verbose=false)

    n = size(X, 1)
    d_x = size(X, 2)
    d_y = size(Y, 2)

    if verbose
      println("# Start Nadaraya Watson Hyper Parameter Validation")
      println("**************************************************")
      println("# Problem Parameters")
      println("1. Number of samples n = ", n)
      println("2. Label dimension : ", d_y)
      println("3. Covariate dimension : ", d_y)
      println("## Hyperparameter optimization")
      println("1. Proportion VALIDATION/TOTAL data =", p)
      println("2. Considered Smoothers : ", S_all) 
    end

    # Compute covariance of covariates
    μ = mean(X, dims=1)
    Σ = X'*X/n-μ'*μ + I/n

    Σ = factorize(Σ)
    
    # Distance function
    d = (x1, x2)-> sqrt((collect(x1[:])-collect(x2[:]))'*(Σ\(collect(x1[:])-collect(x2[:]))))[1]
    
    # hyperparameters
    D = [d(X[i, :], μ) for i in 1:n]
    h_all = exp10.(range(log10(minimum(D))-1, stop=log10(maximum(D))+1, length=100))
    
    
    val = sample(1:n, round(Int, n*p), replace=false)
    train = setdiff(1:n, val)
    
    val_errors = []
    for S in S_all, hn in h_all

        Sn = Weighter(S, d, hn)
        if verbose
            println("*****VALIDATION*****")
            println("Smoother function S : ", Sn)
        end

        Yp = nadaraya_watson_learner(Y[train, :], X[train, :], X[val, :], Sn)
            
        push!(val_errors, [sum((Y[val, :]-Yp).^2), Sn])

    end

    val_errors_star = val_errors[argmin([val_errors[i][1] for i in 1:length(val_errors)])]
    
    if verbose
        println("******************************************************")
        println("*** End Nadaraya Watson Hyper Parameter Validation ***")
    end

    return val_errors_star[2]
end

"""
    nadaraya_watson_cv(Y, X[, p, S_all, verbose])

Determine best Nadaraya-Watson learner based on cross validation.

# Arguments

  1. `Y`: Array of observed responses.
  2. `X`: Covariate data.
  3. `f`: Number of folds
  4. `S_all=gaussian_smoother`: Smoothers considered.

# Returns
  1. `Sn`: validated smoother function
"""
function nadaraya_watson_cv(Y, X, f; S_all=[gaussian_smoother], verbose=false)

    n = size(X, 1)
    d_x = size(X, 2)
    d_y = size(Y, 2)

    if verbose
      println("# Start Nadaraya Watson Hyper Parameter Cross Validation")
      println("********************************************************")
      println("## Problem Parameters")
      println("1. Number of samples n = ", n)
      println("2. Label dimension : ", d_y)
      println("3. Covariate dimension : ", d_y)
      println("## Hyperparameter optimization")
      println("1. Number of folds f =", f)
      println("2. Considered Smoothers : ", S_all) 
    end

    # Compute covariance of covariates
    μ = mean(X, dims=1)
    Σ = X'*X/n-μ'*μ + I/n

    Σ = factorize(Σ)
    
    # Distance function
    d = (x1, x2)-> sqrt((collect(x1[:])-collect(x2[:]))'*(Σ\(collect(x1[:])-collect(x2[:]))))[1]
    
    # hyperparameters
    D = [d(X[i, :], μ) for i in 1:n]
    h_all = exp10.(range(max(log10(minimum(D))-1, -1), stop=min(log10(maximum(D))+1, 0), length=20))
    
    # k Fold cross validation
    folds = collect(Kfold(n, f))
    
    val_errors = []
    for S in S_all, hn in h_all

        Sn = Weighter(S, d, hn)
        println("*****CROSS-VALIDATION*****")
        println("Smoother function S : ", Sn)

        val_error = 0
        for train in folds

            val = setdiff(1:n, train)
            Yp = nadaraya_watson_learner(Y[train, :], X[train, :], X[val, :], Sn)
            
            val_error = val_error + sum((Y[val, :]-Yp).^2)/f

        end
        push!(val_errors, [val_error, Sn])

    end

    val_errors_star = val_errors[argmin([val_errors[i][1] for i in 1:length(val_errors)])]
    
    if verbose
        println("************************************************************")
        println("*** End Nadaraya Watson Hyper Parameter Cross Validation ***")
    end

    return val_errors_star[2]
end

"""
    nadaraya_watson_bootstrap_primal(L, X, Sn, xbar, r)

# Arguments
    1. `L` : Array of observed costs
    2. `X` : Covariate data
    3. `Sn` : Weighter function
    4. `xbar` : Context of interest
    5. 'r' : Robustness parameter
    6. 'verbose' : Verbosity
              
            
# Returns
    1. `c_r` : Robust cost
    2.  '(P^nw, s^nw)' : Optimal primal variables 
    3. 's' : Solver status
"""
function nadaraya_watson_bootstrap_primal(L, X, Sn, xbar, r; verbose=false)
    
    # Problem dimensions
    n = size(L, 1)
    d_y = size(L, 2)
    
    if verbose
      println("# Start Nadaraya Watson Primal Robust Formulation")
      println("*************************************************")
      println("## Problem dimensions")
      println("1. Number of Samples n = ", n)
      println("2. Label dimension : ", d_y)
      println("## Hyper parameters")
      println("1. Weighter Sn : ", Sn)
      println("2. Robustness r = ", r)
    end

    # Primal Variables
    s = Variable(1)
    P = Variable(n)

    t = sum([Sn(X[i, :], xbar) for i in 1:n])
    
    obj = sum([L[i]*(Sn(X[i, :], xbar)/t)*P[i] for i in 1:n])/n

    constrs = [P>=0, sum(P)==s*n, sum([Sn(X[i, :], xbar)/t*P[i] for i in 1:n])==n]
    constrs = constrs + [relative_entropy(P, s*ones(n)) <= s*r*n]

    problem = maximize(obj, constrs)

    solve!(problem, ECOSSolver(maxit=100, abstol=1e-12, verbose=verbose))
    # if problem.status!=:Optimal
    #     solve!(problem, SCSSolver(max_iters=50000, reltol=1e-12), warmstart=true)
    # end

    if verbose
        println("*****************************************************")
        println("*** End Nadaraya Watson Primal Robust Formulation ***")
    end

    return problem.optval, P.value, s.value, problem.status
    
end

"""
    nadaraya_watson_bootstrap_dual(L, X, Sn, xbar, r[, verbose])

# Arguments
    1. `L` : Array of observed costs
    2. `X` : Covariate data
    3. `Sn` : Weighter function
    4. `xbar` : Context of interest
    5. 'r' : Robustness parameter
    6. 'verbose' : Verbosity
              
            
# Returns
    1. `c_r` : Robust cost
    2. 's' : Solver status
"""
function nadaraya_watson_bootstrap_dual(L, X, Sn, xbar, r; verbose=false)
    
    # Problem dimensions
    n = size(L, 1)
    d_y = size(L, 2)

    if verbose
        println("*** Start Nadaraya Watson Dual Robust Formulation ***")
        println("*****************************************************")
        println("Number of Samples n = ", n)
        println("Label dimension : ", d_y)
    end

    t = sum([Sn(X[i, :], xbar) for i in 1:n])

    # Dual Variables
    α = Variable(1)
    ν = Variable(1)
    u = Variable(n)
    
    constrs = [ν>=0, u>=0]
    constrs = constrs + [sum(u) <= ν*exp(-r)]
    for i in 1:n
        constrs = constrs + [(L[i]-α)*Sn(X[i, :], xbar)/t <= log_perspective(n*u[i], ν)]
    end

    problem = minimize(α, constrs)

    solve!(problem, ECOSSolver(maxit=100, abstol=1e-12, verbose=verbose))
    # if problem.status!=:Optimal
    #     solve!(problem, SCSSolver(max_iters=50000, reltol=1e-12, verbose=verbose), warmstart=true)
    # end
    

    if verbose
        println("***************************************************")
        println("*** End Nadaraya Watson Dual Robust Formulation ***")
    end

    return problem.optval, problem.status

end

"""
    nadaraya_watson_bs(Y, X, xbar, Sn[, n, m, verbose])

# Arguments

    1. `Y` : Array of observed responses
    2. `X` : Covariate data
    3. `xbar` : Context of interest
    4. `Sn` : Weighter function
    5. `(n, m)` : Bootstrap samples / multiplicity
    6. 'verbose' : Verbosity

# Returns
    1. `Yp_boot` : NW Prediction on m bootstrap resamples involving n data points
  
"""
function nadaraya_watson_bs(Y, X, xbar, Sn; n=-1, m=1000, verbose=false)

    d_y = size(Y, 2)
    N = size(X, 1)

    if n<0
        n=N
    end
    
    if verbose
        println("# Start Nadaraya Watson Bootstrap")
        println("*********************************")
        println("1. Total number of points : ", N)
        println("2. Number of samples per bootstrap : ", n)
        println("3. Total number of bootstraps : ", m)
    end

    Yp_boot =zeros(m, d_y)
    for i in 1:m

        if verbose && (i % 1000 == 0)
            println("Bootstrap Epoch : ", i)
        end

        bs = wsample(1:N, ones(N)/N, n)
        Yp_boot[i, :] = nadaraya_watson_learner(Y[bs, :], X[bs, :], xbar, Sn)
    end

    if verbose
        println("***End Nadaraya Watson Bootstrap***")
        println("***********************************")          
    end

    return Yp_boot
end
