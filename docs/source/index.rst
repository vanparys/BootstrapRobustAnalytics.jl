.. Bootstrap Robust Analytics documentation master file, created by
   sphinx-quickstart on Sat Nov 25 17:34:33 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
   
======================================================
   Bootstrap Robust Analytics
======================================================

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   introduction
   nw-analytics/nw-analytics
   nn-analytics/nn-analytics
   smoothers/smoothers
   examples/examples
   
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
