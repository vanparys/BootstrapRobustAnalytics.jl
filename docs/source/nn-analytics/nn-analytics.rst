.. jl:module:: NNLearning

===========================
Nearest Neighbors Analytics
===========================

Prediction
----------

Prediction of the conditional mean :math:`m(\bar x) \defn \E{\mb M^\star}{Y|X=\bar x}` using generalized nearest neighbors prediction using the weighter :math:`S_n` based on supervised training data :math:`\mf M_{n, t} \defn [(x_1, y_1), \dots, (x_n, y_n)]`.

The hyperparameters of nearest neighbors learning are the distance metric, the number of nearest neighbors and the weighter function. Classical nearest neighbors learning is obtained when using a weighter based on the ``naive_smoother``.
   
.. jl:function:: nearest_neighbors_learner(Y, X, Xbar, d, k, Sn; verbose=false)

   Nearest neighbors prediction.
		 
   :param Y: Label data
   :param X: Covariate data
   :param Xbar: Covariate contexts
   :param d: Distance metric
   :param k: Number of nearest neighbors
   :param Sn: Weighter
   :kwparam verbose: Verbosity

Hyperparameter Selection
------------------------

The hyperparameters of nearest neighbors learning can be learned from data using simple validation or crossvalidation. In all cases the distance metric :math:`d` is constructed as the Mahalobis distance

.. math::
   d(m=(x, y), \bar x) = (x-\bar x)\tpose \Sigma_{n, t}^{-1} (x-\bar x)
   
where :math:`\Sigma_{n, t}` the empirical variance of the provided training data.

.. jl:function:: nearest_neighbors_v(Y, X; p=0.2, S_all=[naive_smoother], verbose=false)

   :param Y: Label data
   :param X: Covariate data
   :kwparam p: Ratio validation data to total data
   :kwparam S_all: Smoother functions to consider
   :kwparam verbose: Verbosity

   :returns d: Distance metric
   :returns k: Number of nearest neighbors
   :returns Sn: Weighter function
	     
.. jl:function:: nearest_neighbors_cv(Y, X, f; S_all=[naive_smoother], verbose=false)

   :param Y: Label data
   :param X: Covariate data
   :kwparam f: Number of validation folds
   :kwparam S_all: Smoother functions to consider
   :kwparam verbose: Verbosity

   :returns d: Distance metric
   :returns k: Number of nearest neighbors
   :returns Sn: Weighter function
		 
Robust Prediction
-----------------

.. jl:function:: nearest_neighbors_bootstrap_primal(Y, X, d, k, Sn, xbar, r; verbose=false)

   :param L: Label data
   :param X: Covariate data
   :param d: Distance metric
   :param k: Number of nearest neighbors
   :param Sn: Weighter
   :param xbar: Covariate contexts
   :param r: Robustness parameter
   :kwparam verbose: Verbosity

   :returns y_r: Robust prediction
   :returns (P_r,s_r): Optimal Primal Variables
   :returns s: Solver status

.. jl:function:: nearest_neighbors_bootstrap_dual(Y, X, d, k, Sn, xbar, r; verbose=false)

   :param L: Label data
   :param X: Covariate data
   :param d: Distance metric
   :param k: Number of nearest neighbors
   :param Sn: Weighter
   :param xbar: Covariate contexts
   :param r: Robustness parameter
   :kwparam verbose: Verbosity

   :returns y_r: Robust prediction
   :returns s: Solver status
		


Bootstrap Prediction
--------------------

.. jl:function:: nearest_neighbors_bs(Y, X, xbar, d, k, Sn; n=-1, m=1000, verbose=false)

   :param Y: Label data
   :param X: Covariate data
   :param xbar: Covariate contexts
   :param d: Distance metric
   :param k: Number of nearest neighbors
   :param Sn: Weighter
   :kwparam n: Number of samples in a bootstrap
   :kwparam m: Number of bootstraps
   :kwparam verbose: Verbosity

Example
-------

A small example to illustrate Nadaraya-Watson learning::
  
   using Gadfly
   using DataFrames
   using PDMats
   using Distributions

   using Smoothers
   using NNLearning

   # Data Parameters
   n = 500
   d_x = 1

   # Synthetic Data
   Σ = rand(d_x, d_x)
   Σ = (Σ + Σ')/2
   Σ = Σ - minimum(eig(Σ)[1])*eye(d_x) + eye(d_x)
   D_x = MvNormal(zeros(d_x), PDMat(Σ))
   X = rand(D_x, n)'
   Y = sin(X[:, 1]) + randn(n)*0.3
   Data = DataFrame(X=X[:, 1], Y=Y)
   Xbar = repmat(linspace(-3, 3, 25), 1, d_x)
   Signal = DataFrame(X1=Xbar[:, 1],
                      Y=sin(Xbar[:, 1]),
		      T=repmat(["Signal"],
		      size(Xbar, 1)))

   # Hyperparameter selection
   d, k, S_nn = nadaraya_watson_v(Y, X, p=0.1)

   # Bootstrap
   Y_nw = nearest_neighbors_learner(Y, X, Xbar, d, k, S_nn)
   Y_nw_min = zeros(size(Xbar, 1))
   Y_nw_max = zeros(size(Xbar, 1))
   for i in 1:size(Xbar, 1)
     xbar = Xbar[i, :]'
     Y_nw_b = nearest_neighbors_bs(Y, X, xbar, d, k, S_nn, n=n, m=5000)
     Y_nw_b = Y_nw_b[find(!isnan(Y_nw_b))]
     # quantiles
     Y_nw_min[i] = DataArrays.quantile(Y_nw_b[:], 0.01)
     Y_nw_max[i] = DataArrays.quantile(Y_nw_b[:], 0.99)
   end
   NN = DataFrame(X1=Xbar[:, 1],
                  Y=Y_nw[:],
		  Ymin=Y_nw_min[:],
		  Ymax=Y_nw_max[:],
		  T=repmat(["NN"], size(Xbar, 1)))
   
   P = plot(layer(vcat(NN, Signal), x=:X1, y=:Y, color=:T,
	       Geom.line),
	 layer(NN, x=:X, y=:Y, ymin=:Ymin, ymax=:Ymax, color=:T,
	       Geom.line, Geom.ribbon))
   draw(SVG("nn-test.svg", 9.3inch, 7inch), P)

.. image:: nn-test.svg

