.. jl:module:: NWLearning

=========================
Nadaraya-Watson Learning
=========================

Prediction
----------

Prediction of the conditional mean :math:`m(\bar x) \defn \E{\mb M^\star}{Y|X=\bar x}` using Nadaraya-Watson prediction using the weighter funciton :math:`S_n` based on supervised training data :math:`\mf M_{n, t} \defn [(x_1, y_1), \dots, (x_n, y_n)]`.

The hyperparameter of Nadarya-Watson learning is the weighter function :math:`S_n` employed.

.. jl:function:: nadaraya_watson_learner(Y, X, Xbar, Sn; verbose=false)

   Nadaraya-Watson Prediction

   :param Y: Label data
   :param X: Covariate data
   :param Xbar: Covariate contexts
   :param Sn: Weighter
   :kwparam verbose: Verbosity
		 
Hyperparameter Selection
------------------------

A good weighter function for Nadarya-Watson learning can be learned from data using simple validation or crossvalidation.

.. jl:function:: nadaraya_watson_v(Y, X; p=0.2, S_all=[gaussian_smoother], verbose=false)

   Select Nadarya-Watson Hyperparameters using single training / validation split

   :param Y: Label data
   :param X: Covariate data
   :param Xbar: Covariate contexts
   :kwparam p: Ratio validation data to total data
   :kwparam S_all: Smoother functions to consider
   :kwparam verbose: Verbosity

   :returns d: Distance metric
   :returns k: Number of nearest neighbors
   :returns Sn: Weighter function


.. jl:function:: nadaraya_watson_cv(Y, X, f; S_all=[gaussian_smoother], verbose=false)

   Select Nadarya-Watson Hyperparameters using several validation folds
		 
   :param Y: Label data
   :param X: Covariate data
   :kwparam f: Number of validation folds
   :kwparam S_all: Smoother functions to consider
   :kwparam verbose: Verbosity

   :returns d: Distance metric
   :returns k: Number of nearest neighbors
   :returns Sn: Weighter function

		 
Robust Prediction
-----------------

.. jl:function:: nadaraya_watson_bootstrap_primal(L, X, Sn, xbar, r; verbose=false)

   :param L: Label data
   :param X: Covariate data
   :param Sn: Weighter
   :param xbar: Covariate contexts
   :param r: Robustness parameter
   :kwparam verbose: Verbosity

   :returns y_r: Robust prediction
   :returns (P_r,s_r): Optimal Primal Variables
   :returns s: Solver status

.. jl:function:: nadaraya_watson_bootstrap_dual(L, X, Sn, xbar, r; verbose=false)

   :param L: Label data
   :param X: Covariate data
   :param Sn: Weighter
   :param xbar: Covariate contexts
   :param r: Robustness parameter
   :kwparam verbose: Verbosity

   :returns y_r: Robust prediction
   :returns s: Solver status


Bootstrap
---------

.. jl:function:: nadaraya_watson_bs(Y, X, xbar, Sn; n=-1, m=1000, verbose=false)

   :param Y: Label data
   :param X: Covariate data
   :param xbar: Covariate contexts
   :param Sn: Weighter
   :kwparam n: Number of samples in a bootstrap
   :kwparam m: Number of bootstraps
   :kwparam verbose: Verbosity
   

Example
-------

A small example to illustrate Nadaraya-Watson learning::
  
   using Gadfly
   using DataFrames
   using PDMats
   using Distributions

   using Smoothers
   using NWLearning

   # Data Parameters
   n = 500
   d_x = 1

   # Synthetic Data
   Σ = rand(d_x, d_x)
   Σ = (Σ + Σ')/2
   Σ = Σ - minimum(eig(Σ)[1])*eye(d_x) + eye(d_x)
   D_x = MvNormal(zeros(d_x), PDMat(Σ))
   X = rand(D_x, n)'
   Y = sin(X[:, 1]) + randn(n)*0.3
   Data = DataFrame(X=X[:, 1], Y=Y)
   Xbar = repmat(linspace(-3, 3, 25), 1, d_x)
   Signal = DataFrame(X1=Xbar[:, 1],
                      Y=sin(Xbar[:, 1]),
		      T=repmat(["S"],
		      size(Xbar, 1)))

   # Hyperparameter selection
   S_nw = nadaraya_watson_v(Y, X, p=0.1)

   # Bootstrap
   Y_nw = nadaraya_watson_learner(Y, X, Xbar, S_nw)
   Y_nw_min = zeros(size(Xbar, 1))
   Y_nw_max = zeros(size(Xbar, 1))
   for i in 1:size(Xbar, 1)
     xbar = Xbar[i, :]'
     Y_nw_b = nadaraya_watson_bs(Y, X, xbar, S_nw, n=n, m=5000)
     Y_nw_b = Y_nw_b[find(!isnan(Y_nw_b))]
     # quantiles
     Y_nw_min[i] = DataArrays.quantile(Y_nw_b[:], 0.01)
     Y_nw_max[i] = DataArrays.quantile(Y_nw_b[:], 0.99)
   end
   NW = DataFrame(X1=Xbar[:, 1],
                  Y=Y_nw[:],
		  Ymin=Y_nw_min[:],
		  Ymax=Y_nw_max[:],
		  T=repmat(["NW"], size(Xbar, 1)))
   
   P = plot(layer(vcat(NW, Signal), x=:X1, y=:Y, color=:T,
	       Geom.line),
	 layer(NW, x=:X, y=:Y, ymin=:Ymin, ymax=:Ymax, color=:T,
	       Geom.line, Geom.ribbon))
   draw(SVG("nw-test.svg", 9.3inch, 7inch), P)

.. image:: nw-test.svg
