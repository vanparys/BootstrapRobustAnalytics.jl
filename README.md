# Bootstrap Robust Analytics with Julia

* Abstract

We address the problem of prescribing an optimal decision in a framework where its cost depends on uncertain problem parameters \(Y\) that need to be learned from data. Earlier work transformed classical machine learning methods that merely predict \( Y \) from supervised training data \([(x_1, y_1), \dots, (x_n, y_n)]\) into prescriptive methods taking optimal decisions specific to a particular covariate context \(X=\bar x\). Their prescriptive methods factor in additional observed contextual information on a potentially large number of covariates \(X=\bar x\) to take context specific actions \(z(\bar x)\) which are superior to any static decision $z$. Any naive use of limited training data may, however, lead to gullible decisions over-calibrated to one particular data set. In this paper, we borrow ideas from distributionally robust optimization and the statistical bootstrap of Efron to propose two novel prescriptive methods based on (nw) Nadaraya-Watson and (nn) nearest-neighbors learning which safeguard against overfitting and lead to improved out-of-sample performance. Both resulting robust  prescriptive methods reduce to tractable convex optimization problems and enjoy a limited disappointment on bootstrapped data.

* Links

ArXiv paper : https://arxiv.org/abs/1711.09974
