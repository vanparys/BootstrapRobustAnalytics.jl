##############
## Packages ##
##############

using Random
using LinearAlgebra
using CSV

using BootstrapRobustAnalytics
using NWPortfolio



Random.seed!(12321)

###############
## LOAD DATA ##
###############
X_data = CSV.read("X_nt.csv")
Y_data = CSV.read("Y_nt.csv")

###################
## COST FUNCTION ##
###################
LOSS(z, y) = -dot(z, y)

################
## Split data ##
################
perm = shuffle(1:size(X_data, 1))
X_data = X_data[perm, :]
Y_data = Y_data[perm, :]

n = 250

X = convert(Matrix, X_data)[1:n, :]
Y = convert(Matrix, Y_data)[1:n, :]
xbar = convert(Matrix, X_data)[n+7, :]

#############################
## Hyperparameter training ##
#############################
S_all=[gaussian_smoother]
Sn = nadaraya_watson_cv(Y, X, 10, verbose=true, S_all=S_all)

#########################
## Bootstrap Analytics ##
#########################
b = 0.1

##########################
# NW nominal formulation #
##########################

(c_n, z_n, s_n) = portfolio_nw_nominal(Y, X, Sn, xbar)
println("Nominal Decision: ", z_n)

#########################
# NW robust formulation #
#########################
# Robustness radius Nadaraya Watson
r = 1/n*log(1/b)

(c_r, z_r, s_r) = portfolio_nw_bootstrap(Y, X, Sn, xbar, r)
println("Robust Decision: ", z_r)

###############
## Dual Test ##
###############

L = [LOSS(z_r[:], Y[i, :]) for i in 1:size(Y, 1)]

c_r_p, P, s, s_r_p = nadaraya_watson_bootstrap_primal(L, X, Sn, xbar, r)
c_r_d, s_r_d = nadaraya_watson_bootstrap_dual(L, X, Sn, xbar, r)

println("Nominal cost: ", c_n, ", Status: ", s_n)
println("Robust cost: ", c_r, ", Status: ", s_r)
println("Robust cost (primal): ", c_r_p, ", Status: ", s_r_p)
println("Robust cost (dual): ", c_r_d, ", Status: ", s_r_d)
