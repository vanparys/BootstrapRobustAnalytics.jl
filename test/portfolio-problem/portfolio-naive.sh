#!/bin/bash
#SBATCH -a 1-10
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=16000
#SBATCH --time=0-6:00:00
#SBATCH -p sched_mit_sloan_batch

srun julia portfolio-naive.jl $SLURM_ARRAY_TASK_ID
