srand(123)

##############
## Packages ##
##############
using BootstrapRobustAnalytics
using CSV

using NNPortfolio
https://www.facebook.com/
###############
## LOAD DATA ##
###############
X_data = Array(CSV.read("X_nt.csv"))
Y_data = Array(CSV.read("Y_nt.csv"))
# random tie breaking
X_data = X_data + rand(size(X_data))/10

###################
## COST FUNCTION ##
###################

LOSS(z, y) = -dot(z, y)

################
## Split data ##
################
perm = shuffle(1:size(X_data, 1))
X_data = X_data[perm, :]
Y_data = Y_data[perm, :]

n = 250

X = Array(X_data)[1:n, :]
Y = Array(Y_data)[1:n, :]
xbar = Array(X_data)[n+2, :]

# #############################
# ## Hyperparameter training ##
# #############################

d, k, Sn = nearest_neighbors_cv(Y, X, 10, verbose=true)

##############################
## Performance on test data ##
##############################
b = 0.2

##########################
# NN nominal formulation #
##########################
(c_n, z_n, s_n) = portfolio_nn_nominal(Y, X, d, k, Sn, xbar, verbose=false)

#########################
# NN robust formulation #
#########################
# Robustness radius
r = 1/n*log(1/b)

(c_r, z_r, s_r) = portfolio_nn_bootstrap(Y, X, d, k, Sn, xbar, r, verbose=false)

###############
## Dual Test ##
###############

L = [LOSS(z_r[:], Y[i, :]) for i in 1:size(Y, 1)]

c_r_p, P, s, j_r, s_r_p = nearest_neighbors_bootstrap_primal(L, X, d, k, Sn, xbar, r)
c_r_d, s_r_d = nearest_neighbors_bootstrap_dual(L, X, d, k, Sn, xbar, r)

println("Nominal cost: ", c_n, ", Status: ", s_n)
println("Robust cost: ", c_r, ", Status: ", s_r)
println("Robust cost (primal): ", c_r_p, ", Status: ", s_r_p)
println("Robust cost (dual): ", c_r_d, ", Status: ", s_r_d)
