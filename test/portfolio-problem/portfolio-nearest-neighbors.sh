#!/bin/bash
#SBATCH -a 201-400
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=32000
#SBATCH --time=0-5:00:00
#SBATCH -p sched_mit_sloan_batch

srun julia portfolio-nearest-neighbors.jl $SLURM_ARRAY_TASK_ID
