#+TITLE:    Mean-CVaR Portfolio Allocation Problem
#+AUTHOR:   Bart Van Parys
#+EMAIL:    vanparys@mit.edu
#+OPTIONS:  H:2
#+PROPERTY: exports code
#+PROPERTY: results silent
#+PROPERTY: session *julia*
#+PROPERTY: tangle yes
#+PROPERTY: eval never

#+BEGIN_SRC julia :exports none
  module NWPortfolio

  export portfolio_nw_nominal
  export portfolio_nw_bootstrap
#+END_SRC

We consider a portfolio allocation problem in which the decision \( z\in \Re_+^{d_y} \) consists in how a limited investment budget is split among \( d_y \) securities. The returns \( Y \) that each of the securities will provie are evidently uncertain and not known ahead of time. Before any investment needs to be made, it would be wise to take into account a number of covariates $X$ observed to take the particular value $\bar x$. Investment must balance

- returns : \( z^\top Y \), and
- losses : \( \max(-z^\top Y, 0) \).

Using a trade-off \( \lambda \) between risks as measured by the CVaR\(_\epsilon\) and mean returns, the classical formulation of the portfolio allocation problem reads

\begin{equation*}
  \begin{array}{rl}
  (z^\star(\bar x), b^\star(\bar x))\in\arg\inf & \mathbb E_{\mathbb M^\star} \big[ L(z, b, Y) := b + (-z^\top Y - b)^+/\epsilon -\lambda \cdot z^\top Y \big| X = \bar x\big]\\
       & z \in \Re_+, b \in \Re, \\
       & \mathbb 1 ^\top z = 1.
  \end{array}
\end{equation*}

This classical portfolio allocation formulation assumes the joint distribution \( \mathbb M^\star \) between returns and covariates to be known. In practice however this is almost never the case.

* Dependencies
  
#+BEGIN_SRC julia
using ECOS
using Convex
using SCS

importall NWLearning
#+END_SRC

* Nominal Nadaraya-Watson Formulation

Here, we will conider a *Nadaraya-Watson learner* to estimate the distribution of the returns \( Y \) conditional on \(X=\bar x\). Simply using the Nadaraya-Watson estimate for the distribution of \(Y\) in the covariate context \( X=\bar x\) gives us the following data-driven nominal formulation of the portfolio allocation problem

\begin{equation*}
  \begin{array}{rl}
  (z^{\mathrm{nw}}_{n, t}(\bar x), b^{\mathrm{nw}}_{n, t}(\bar x))\in \arg\inf & \sum S_n(x-\bar x) L(z, b, Y) \mathbb M_{n, t}(x, y)\Big/\sum S_n(x-\bar x)\mathbb M_{n, t}(x, y)\\
       & z \in \Re_+, b \in \Re, \\
       & \mathbb 1 ^\top z = 1.
  \end{array}
\end{equation*}

The Nadaraya-Watson formulation is implemented numerically using the CONVEX.jl parser.

#+BEGIN_SRC julia
  """
      portfolio_nw_nominal(Y, X, Sn, xbar, ϵ, λ[, verbose])

  # Arguments

      1. 'Y' : Observational data
      2. 'X' : Covariate data
      3. 'Sn' : Weighter function
      4. 'xbar' : Context of interest
      5. '(ϵ, λ)' : Portfolio allocation parameters
      6. 'verbose' : Verbosity

  # Returns

      1. 'val' : Optimal Cost
      2. '(z, b)' : Optimal Action
      3. 's' : Solver status
  """
  function portfolio_nw_nominal(Y, X, Sn, xbar, ϵ, λ; verbose=false)

      n = size(Y, 1)
      d_y = size(Y, 2)
      
      if verbose
          println("# Start Portfolio Nominal NW Formulation")
          println("******************************************")
          println("## Problem Parameters")
          println("1. Risk level CVaR ϵ = ", ϵ)
          println("2. Risk / Reward trade off λ = ", λ)
          println("## Problem dimensions ")
          println("1. Number of samples n = ", n)
          println("2. Label dimension : ", d_y)
          println("## Hyper parameters")
          println("1. Weighter function Sn = ", Sn)
      end


      # Primal Variables
      z = Variable(d_y)
      L = Variable(n)
      b = Variable(1)

      obj = sum([L[i]*Sn(X[i, :], xbar) for i in 1:n])/sum([Sn(X[i, :], xbar) for i in 1:n])

      constrs = [z>=0, sum(z)==1]
      for i in 1:n
          constrs = constrs + [L[i] >= (1-1/ϵ)*b - (λ+1/ϵ)*dot(Y[i, :], z)]
          constrs = constrs + [L[i] >= b - λ*dot(Y[i, :], z)]
      end

      problem = minimize(obj, constrs)
      solve!(problem, ECOSSolver(verbose=verbose))

      if verbose
          println("*** End Portfolio Nominal NN Formulation ***")
          println("**********************************************")
      end
      
      return problem.optval, z.value, b.value, problem.status
  end
#+END_SRC

* Bootstrap Robust Formulation

We consider the model distance function \[ B(\mathbb M, \mathbb M_{n, t}) := \sum \mathbb M(x, y) \log \frac{\mathbb M(x, y)}{\mathbb M_{n, t}(x, y)}.\] The robust counterpart of the nominal Nadaraya-Watson formulation with respect to the previously defined model distance function can be presented via its convex dual as

\begin{equation*}
  \begin{array}{rl}
  (z^{r, \mathrm{nw}}_{n, t}(\bar x), b^{r, \mathrm{nw}}_{n, t}(\bar x))\in \arg\inf & \alpha \\
       & z \in \Re_+, b \in \Re, \alpha\in\Re, \nu \in \Re_{++}\\
       & \nu \log \big(\sum \exp((L(z, b, y)-\alpha) S_n(x - \bar x) / \nu)~ \mathbb M_{n, t}(x, y)\big) + r\nu \leq 0, \\
       & \mathbb 1 ^\top z = 1.
  \end{array}
\end{equation*}

The robust Nadaraya-Watson formulation is implemented numerically using the CONVEX.jl parser.

#+BEGIN_SRC julia
  """
      portfolio_nw_bootstrap(Y, X, Sn, xbar, ϵ, λ, r[, verbose])

  # Arguments

      1. 'Y' : Observational data
      2. 'X' : Covariate data
      3. 'Sn' : Weighter function
      4. 'xbar' : Context of interest
      5. '(ϵ, λ)' : Portfolio allocation parameters
      6. 'r' : Robustness parameter
      7. 'verbose' : Verbosity

  # Returns

      1. 'val' : Optimal Cost
      2. '(z, b)' : Optimal Action
      3. 's' : Solver status
  """
  function portfolio_nw_bootstrap(Y, X, Sn, xbar, ϵ, λ, r; verbose=false)

      n = size(Y, 1)
      d_y = size(Y, 2)

      if verbose
          println("# Start News Vendor Robust NW Formulation")
          println("*****************************************")
          println("## Problem Parameters")
          println("1. Risk level CVaR ϵ = ", ϵ)
          println("2. Risk / Reward trade off λ = ", λ)
          println("## Problem dimensions ")
          println("1. Number of samples n = ", n)
          println("2. Label dimension : ", d_y)
          println("3. Robustness parameter r = ", r)
          println("## Hyper parameters")          
          println("1. Weighter function Sn = ", Sn)
      end

      s = sum([Sn(X[i, :], xbar) for i in 1:n])/n

      # Primal variables
      z = Variable(d_y)
      b = Variable(1)
      L = Variable(n)

      # Dual variables
      α = Variable(1)
      ν = Variable(1)
      u = Variable(n)
      
      constrs = [z>=0, sum(z)==1, ν>=0, u>=0]
      for i in 1:n
          constrs = constrs + [L[i] >= (1-1/ϵ)*b - (λ+1/ϵ)*dot(Y[i, :], z)]
          constrs = constrs + [L[i] >= b - λ*dot(Y[i, :], z)]
      end

      constrs = constrs + [sum(u) <= ν*exp(-r)]
      for i in 1:n
          constrs = constrs + [(L[i]-α)*Sn(X[i, :], xbar)/s <= log_perspective(n*u[i], ν)]
      end

      problem = minimize(α, constrs)

      solve!(problem, ECOSSolver(maxit=1000, reltol=1e-12, verbose=verbose))
      # if problem.status!=:Optimal
      #     solve!(problem, SCSSolver(max_iters=50000, reltol=1e-6, verbose=verbose), warmstart=true)
      # end

      if verbose
          println("*** End Portfolio Robust NN Formulation ***")
          println("**********************************************")
      end
      
      return problem.optval, z.value, b.value, problem.status
  end
#+END_SRC

#+BEGIN_SRC julia :exports none
  #END MODULE
  end
#+END_SRC

* Example

#+BEGIN_SRC julia :tangle no :eval yes
  using DataFrames
  importall NWLearning
  importall NWPortfolio

  # Problem Parameters
  ϵ = 0.05
  λ = 0.1
  xbar = [0, 0]

  # Data
  n=500
  X = randn(n, 2)
  Y = randn(n, 3)
#+END_SRC

#+BEGIN_SRC julia :tangle no :eval yes
  # Parameter selection
  Sn = nadaraya_watson_v(Y, X, verbose=true)
#+END_SRC
  
#+BEGIN_SRC julia :tangle no :eval yes
  # Nominal formulation
  R = DataFrame(Value=[], R=[], Status=[])
  v_n, z_n, b_n, s_n = portfolio_nw_nominal(Y,X,Sn,xbar,ϵ,λ, verbose=true)
  push!(R, [v_n, 0.0, s_n])
#+END_SRC

#+BEGIN_SRC julia :tangle no :eval yes :results silent
  # Robust formulation
  for r in vcat([0], logspace(-4, -1, 5))
      v_r, z_r, b_r, s_r = portfolio_nw_bootstrap(Y,X,Sn,xbar,ϵ,λ,r,verbose=true)
      push!(R, [v_r, r, s_r])
  end
#+END_SRC

#+BEGIN_SRC julia :tangle no :eval yes :results output :exports results
  # Nominal formulation
  println(R)
#+END_SRC

#+RESULTS:
#+begin_example

7×3 DataFrames.DataFrame
│ Row │ Value   │ R           │ Status  │
├─────┼─────────┼─────────────┼─────────┤
│ 1   │ 1.26223 │ 0.0         │ Optimal │
│ 2   │ 1.26225 │ 0.0         │ Optimal │
│ 3   │ 1.28421 │ 0.0001      │ Optimal │
│ 4   │ 1.31504 │ 0.000562341 │ Optimal │
│ 5   │ 1.39055 │ 0.00316228  │ Optimal │
│ 6   │ 1.55614 │ 0.0177828   │ Optimal │
│ 7   │ 1.73885 │ 0.1         │ Optimal │
#+end_example
