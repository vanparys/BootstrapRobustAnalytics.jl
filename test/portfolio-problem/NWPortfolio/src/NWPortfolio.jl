module NWPortfolio

export portfolio_nw_nominal
export portfolio_nw_bootstrap

using ECOS
using Convex
using SCS

using BootstrapRobustAnalytics

"""
    portfolio_nw_nominal(Y, X, Sn, xbar, ϵ, λ[, verbose])

# Arguments

    1. 'Y' : Observational data
    2. 'X' : Covariate data
    3. 'Sn' : Weighter function
    4. 'xbar' : Context of interest
    5. 'verbose' : Verbosity

# Returns

    1. 'val' : Optimal Cost
    2. 'z' : Optimal Action
    3. 's' : Solver status
"""
function portfolio_nw_nominal(Y, X, Sn, xbar; verbose=false)

    n = size(Y, 1)
    d_y = size(Y, 2)
    
    if verbose
        println("# Start Portfolio Nominal NW Formulation")
        println("******************************************")
        println("## Problem dimensions ")
        println("1. Number of samples n = ", n)
        println("2. Label dimension : ", d_y)
        println("## Hyper parameters")
        println("1. Weighter function Sn = ", Sn)
    end


    # Primal Variables
    z = Variable(d_y)
    L = Variable(n)
    b = Variable(1)

    obj = sum([-dot(Y[i, :], z)*Sn(X[i, :], xbar) for i in 1:n])/sum([Sn(X[i, :], xbar) for i in 1:n])
    constrs = [z>=0, sum(z)==1]

    problem = minimize(obj, constrs)
    solve!(problem, ECOSSolver(verbose=verbose))

    if verbose
        println("*** End Portfolio Nominal NN Formulation ***")
        println("**********************************************")
    end
    
    return problem.optval, z.value, problem.status
end

"""
    portfolio_nw_bootstrap(Y, X, Sn, xbar, ϵ, λ, r[, verbose])

# Arguments

    1. 'Y' : Observational data
    2. 'X' : Covariate data
    3. 'Sn' : Weighter function
    4. 'xbar' : Context of interest
    5. 'r' : Robustness parameter
    6. 'verbose' : Verbosity

# Returns

    1. 'val' : Optimal Cost
    2. 'z' : Optimal Action
    3. 's' : Solver status
"""
function portfolio_nw_bootstrap(Y, X, Sn, xbar, r; verbose=false)

    n = size(Y, 1)
    d_y = size(Y, 2)

    if verbose
        println("# Start News Vendor Robust NW Formulation")
        println("*****************************************")
        println("## Problem dimensions ")
        println("1. Number of samples n = ", n)
        println("2. Label dimension : ", d_y)
        println("3. Robustness parameter r = ", r)
        println("## Hyper parameters")          
        println("1. Weighter function Sn = ", Sn)
    end

    s = sum([Sn(X[i, :], xbar) for i in 1:n])/n

    # Primal variables
    z = Variable(d_y)

    # Dual variables
    α = Variable(1)
    ν = Variable(1)
    u = Variable(n)
    
    constrs = [z>=0, sum(z)==1, ν>=0, u>=0]
    constrs = constrs + [sum(u) <= ν*exp(-r)]
    for i in 1:n
        constrs = constrs + [(-dot(Y[i, :], z)-α)*Sn(X[i, :], xbar)/s <= log_perspective(n*u[i], ν)]
    end

    problem = minimize(α, constrs)

    solve!(problem, ECOSSolver(maxit=2000, reltol=1e-12, verbose=verbose))
    # if problem.status!=:Optimal
    #     solve!(problem, SCSSolver(max_iters=50000, reltol=1e-6, verbose=verbose), warmstart=true)
    # end

    if verbose
        println("*** End Portfolio Robust NN Formulation ***")
        println("**********************************************")
    end
    
    return problem.optval, z.value, problem.status
end

#END MODULE
end
