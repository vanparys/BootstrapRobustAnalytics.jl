using DataFrames
using CSV
using Gadfly

###################################
## Covariates from google trends ##
###################################

#########################
## Cambridge Analytica ##
#########################

ca = CSV.read("Data/google-trends-cambridge-analytica.csv")
ca[:int] = cumsum(ca[:search])
ca[:y] = 1
ca[find(s->s<100, ca[:int]), :y] = 0
ca = ca[end:-1:1, :]

plot(ca, x=:Week, y=:search, Geom.point, Geom.line) |> PDF("CA.pdf")

############
## REDHAT ##
############

redhat = CSV.read("Data/google-trends-ibm-redhat.csv")
redhat[:int] = cumsum(max.(redhat[:search]-5, 0))
redhat[:y] = 1
redhat[find(s->s<20, redhat[:int]), :y] = 0
redhat = redhat[end:-1:1, :]

plot(redhat, x=:Week, y=:int, Geom.point, Geom.line) |> PDF("RH.pdf")

X = hcat(DataFrame(ca=ca[:y]), DataFrame(redhat=redhat[:y]))
X[:week] = 1:size(X, 1)

######################################
## Market data from alphavantage.co ##
######################################

#########
## IBM ##
#########
ibm = CSV.read("Data/weekly_IBM.csv")
ibm=ibm[find(s->s>Dates.Date(2014, 2, 2), ibm[:timestamp]), [:timestamp, :open, :close]]
ibm[:timestamp]=[Dates.toprev(d->Dates.dayname(d) == "Sunday", ibm[i, 1]) for i in 1:size(ibm, 1)]
ibm[:return]=ibm[:close]-ibm[:open]
ibm[:name]="IBM"

CSV.write("ibm.csv", ibm[:, [:timestamp, :close]], header=false)

############
## AIRBUS ##
############
air = CSV.read("Data/weekly_AIR.csv")
air=air[find(s->s>Dates.Date(2014, 2, 2), air[:timestamp]), [:timestamp, :open, :close]]
air[:timestamp]=[Dates.toprev(d->Dates.dayname(d) == "Sunday", air[i, 1]) for i in 1:size(air, 1)]
air[:return]=air[:close]-air[:open]
air[:name]="AIR"

CSV.write("air.csv", air[:, [:timestamp, :close]], header=false)

############
## BOEING ##
############
ba = CSV.read("Data/weekly_BA.csv")
ba=ba[find(s->s>Dates.Date(2014, 2, 2), ba[:timestamp]), [:timestamp, :open, :close]]
ba[:timestamp]=[Dates.toprev(d->Dates.dayname(d) == "Sunday", ba[i, 1]) for i in 1:size(ba, 1)]
ba[:return]=ba[:close]-ba[:open]
ba[:name]="BA"

CSV.write("ba.csv", ba[:, [:timestamp, :close]], header=false)

##############
## FACEBOOK ##
##############
fb = CSV.read("Data/weekly_FB.csv")
fb=fb[find(s->s>Dates.Date(2014, 2, 2), fb[:timestamp]), [:timestamp, :open, :close]]
fb[:timestamp]=[Dates.toprev(d->Dates.dayname(d) == "Sunday", fb[i, 1]) for i in 1:size(fb, 1)]
fb[:return]=fb[:close]-fb[:open]
fb[:name]="FB"

CSV.write("fb.csv", fb[:, [:timestamp, :close]], header=false)

Y = hcat(DataFrame(ibm=ibm[:return]), DataFrame(air=air[:return]), DataFrame(ba=ba[:return]), DataFrame(fb=fb[:return]))

CSV.write("X_nt.csv", X)
CSV.write("Y_nt.csv", Y)


####################
## TIME COVARIATE ##
####################
stocks=vcat(ibm, air, ba, fb)
plot(stocks, x=:timestamp, y=:close, color=:name) |> PDF("time-covariate.pdf")

########
## CA ##
########

ca_fb = hcat(ca[:, [:y]], fb[:, [:return]])
by(ca_fb, :y) do df
    DataFrame(m = mean(df[:return]), v = var(df[:return]))
end

############
## Redhat ##
############

redhat_ibm = hcat(redhat[:, [:y]], ibm[:, [:return]])
by(redhat_ibm, :y) do df
    DataFrame(m = mean(df[:return]), v = var(df[:return]))
end


