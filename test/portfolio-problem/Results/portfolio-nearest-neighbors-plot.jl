using DataFrames
using CSV
using Gadfly

R_nn = reduce(vcat, map(CSV.read, filter(x->contains(x, "_nn_"), readdir())))

############
## FILTER ##
############

R_nn_f = R_nn[find(s->s=="Optimal", R_nn[:status]), :]

R_nn_f = R_nn_f[find(s->s>=2, R_nn_f[:j]), :]

R_nn_f = R_nn_f[find(s->abs(s)<=1e-4, R_nn_f[:cost] -R_nn_f[:cost_p]), :]
# R_nn_f = R_nn_f[find(s->abs(s)<=1e-2, R_nn_f[:cost] -R_nn_f[:cost_d]), :]

indices = shuffle(1:61)
val = indices[1:31]
tst = indices[32:end]

R_nn_val = R_nn_f[find(s->s in val, R_nn_f[:ts]), :]
R_nn_tst = R_nn_f[find(s->s in tst, R_nn_f[:ts]), :]


R_nn_val_a = by(R_nn_val, [:b]) do df
    DataFrame(Profit=mean(df[:profit_test]), profit_s2 = std(df[:profit_test])/sqrt(size(df[:profit_test], 1)), m = size(df[:profit_test], 1))
end

R_nn_tst_a = by(R_nn_tst, [:b]) do df
    DataFrame(Profit=mean(df[:profit_test]), profit_s2 = std(df[:profit_test])/sqrt(size(df[:profit_test], 1)), m = size(df[:profit_test], 1))
end


R_nn_val_a[:Robustness]=1./R_nn_val_a[:b]
R_nn_val_a[:Type] = "Validation"
R_nn_tst_a[:Robustness]=1./R_nn_tst_a[:b]
R_nn_tst_a[:Type] = "Test"

R_nn_val_a = R_nn_val_a[sortperm(R_nn_val_a[:b], rev=true), :]
R_nn_tst_a = R_nn_tst_a[sortperm(R_nn_tst_a[:b], rev=true), :]
CSV.write("R-nn-val.csv", R_nn_val_a)
CSV.write("R-nn-tst.csv", R_nn_tst_a)


##########
## PLOT ##
##########

plot(vcat(R_nn_val_a, R_nn_tst_a), x=:Robustness, y=:Profit, color=:Type, Geom.line, Geom.point, Scale.x_log10) |> PDF("NN.pdf")
