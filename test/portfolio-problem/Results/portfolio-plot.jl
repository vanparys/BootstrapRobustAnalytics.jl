using DataFrames
using CSV
using Gadfly

R_nn = CSV.read("R_nn.csv")
R_nw = CSV.read("R_nw.csv")
R_na = CSV.read("R_na.csv")

R_nn[:Method]="nn"
R_nw[:Method]="nw"
R_na[:Method]="na"

R = vcat(R_nn[:, [:Robustness, :Profit, :Method]], R_nw[:, [:Robustness, :Profit, :Method]], R_na[:, [:Robustness, :Profit, :Method]])

# ##########
# ## PLOT ##
# ##########

plot(R, x=:Robustness, y=:Profit, color=:Method, Geom.line, Geom.point, Scale.x_log10) |> PDF("R.pdf")
