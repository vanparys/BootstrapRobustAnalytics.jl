module NNPortfolio

export portfolio_nn_nominal
export portfolio_nn_bootstrap

using ECOS
using Convex

using BootstrapRobustAnalytics

"""
    portfolio_nn_nominal(Y, X, d, k, Sn, xbar[, verbose=false])

# Arguments

      1. 'Y' : Observational data
      2. 'X' : Covariate data
      3. 'd' : Distance metric
      4. 'k' : Number of nearest neighbors
      5. 'Sn' : Weighter function
      6. 'xbar' : Context of interest
      7. 'verbose' : Verbosity

  # Returns

      1. 'val' : Optimal Cost
      2. 'z' : Optimal Action
      3. 's' : Solver status
"""
function portfolio_nn_nominal(Y, X, d, k, Sn, xbar; verbose=false)

    n = size(Y, 1)
    d_y = size(Y, 2)

    if verbose
        println("# Start Portfolio Nominal NN Formulation")
        println("******************************************")
        println("## Problem dimensions ")
        println("1. Number of samples n = ", n)
        println("2. Label dimension : ", d_y)
        println("## Hyper parameters")
        println("1. Number of nearest neighbors k = ", k)
        println("2. Weighter function Sn = ", Sn)
    end
    
    ## SORT the data based on distance to xbar
    dist = [d(X[i, :], xbar) for i in 1:n]
    perm = sortperm(dist)
    dist = dist[perm]
    Y_nn = Y[perm, :]
    X_nn = X[perm, :]

    R_star = dist[k]+1e-7
    Nk = find(s->s<=R_star, dist)
    
    # OPTIMIZATION FORMULATION
    z = Variable(d_y)

    # Objective
    obj = sum([-dot(Y_nn[i, :], z)*Sn(X_nn[i, :], xbar) for i in Nk])/sum([Sn(X_nn[i, :], xbar) for i in Nk])

    # Constraints
    constrs = [z>=0, sum(z)==1]

    problem = minimize(obj, constrs)
    solve!(problem, ECOSSolver(verbose=verbose))
    
    if verbose
        println("*** End Portfolio Nominal NN Formulation ***")
        println("**********************************************")
    end

    return problem.optval, z.value, problem.status
end

"""
    portfolio_nn_bootstrap(Y, X, d, k, Sn, xbar, r[, verbose])

  # Arguments

      1. 'Y' : Observational data
      2. 'X' : Covariate data
      3. 'd' : Distance metric
      4. 'k' : Number of nearest neighbors
      5. 'Sn' : Weighter function
      6. 'xbar' : Context of interest
      7. 'r' : Robustness paramter
      8. 'verbose' : Verbosity

  # Returns

      1. 'val' : Optimal cost
      2. 'z' : Optimal order
      3. 's' : Solver status
"""
function portfolio_nn_bootstrap(Y, X, d, k, Sn, xbar, r; verbose=false)

    n = size(Y, 1)
    d_y = size(Y, 2)

    if verbose
        println("# Start Portfolio Robust NN Formulation")
        println("*****************************************")
        println("## Problem dimensions ")
        println("1. Number of samples n = ", n)
        println("2. Label dimension : ", d_y)
        println("3. Robustness parameter r = ", r)
        println("## Hyper parameters")
        println("1. Number of nearest neighbors k = ", k)
        println("2. Weighter function Sn = ", Sn)
    end
    
    ## SORT the data based on distance to xbar
    dist = [d(X[i, :], xbar) for i in 1:n]
    perm = sortperm(dist)
    dist = dist[perm]
    Y_nn = Y[perm, :]
    X_nn = X[perm, :]

    J = J_feasible(k, n, r)

    ## Optimization
    z = Variable(d_y) 
    α = Variable(1)
    constrs = [z>=0, sum(z)==1]
    
    for j in J
        #
        η = Variable(2)
        η.value = zeros(2)
        ν = Variable(1)
        u = Variable(j+1)
        #
        s_j = sum([Sn(X_nn[i, :], xbar) for i in 1:j])
        #
        constrs = constrs + [u>=0, ν>=0, η>=0]
        #
        constrs = constrs + [sum(u[1:j]) + (n-j)*u[j+1] <= n*ν*exp(-r)]
        for i in 1:j-1
            constrs = constrs + [(-dot(Y_nn[i, :], z)-α)*Sn(X_nn[i, :], xbar)/s_j*n <= log_perspective(n*u[i], n*ν) + (k-n)/k*η[1] + (1-k+n)/k*η[2]]
        end
        constrs = constrs + [(-dot(Y_nn[j, :], z)-α)*Sn(X_nn[j, :], xbar)/s_j*n  <= log_perspective(n*u[j], n*ν) + (k-n)/k*η[1] + (1-k)/k*η[2] ]
        constrs = constrs + [0<=log_perspective(n*u[j+1], n*ν) + η[1] + (1-k)/k*η[2]]
        #
    end
    
    problem = minimize(α, constrs)
    
    solve!(problem, ECOSSolver(maxit=1000, reltol=1e-12, verbose=verbose))
    # if problem.status!=:Optimal
    #     solve!(problem, SCSSolver(max_iters=50000, reltol=1e-6, verbose=verbose), warmstart=true)
    # end

    if verbose
        println("*** End Portfolio Robust NN Formulation ***")
        println("*********************************************")
    end
    
    return problem.optval, z.value, problem.status
end

#END MODULE
end
