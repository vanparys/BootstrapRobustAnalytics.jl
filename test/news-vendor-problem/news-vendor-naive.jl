using DataFrames
using CSV
using Distributions
using PDMats

using BootstrapRobustAnalytics
using NWNewsVendor

include("news-vendor-solution.jl")

runs = 1:5
# SAVE
if length(ARGS)==0
    run=runs[2]
else
    run = runs[parse(ARGS[1])]
end
srand(run+100)

println("run: ", run)

R = DataFrame(run=[], n=[], S=[], h=[], j=[], b=[], c_t=[], c=[], c_p=[], c_d=[], status=[], status_p=[], status_d=[])

###################
## GENERATE DATA ##
###################

n = 200

for _ in 1:100

    # Covariates
    TEMPERATURE = Uniform(-5, 40)
    DAY = Categorical(ones(7)/7)
    X_nt = DataFrame(TEMPERATURE=rand(TEMPERATURE, n), DAY=rand(DAY, n))
    μ_nt = 100 + (X_nt[:TEMPERATURE]-20)*2
    μ_nt[find(s->s>=6, X_nt[:DAY])] = μ_nt[find(s->s>=6, X_nt[:DAY])] + 50
    Y_nt = μ_nt + 4*randn(n)
    Y_data = DataFrame(DEMAND=Y_nt[:, 1])
    X_data = Array(X_nt)
    X_tr = X_data[1:n, :]
    Y_tr = Array(Y_data)[1:n, :]

    #######################################
    ## COST FUNCTION NEWS VENDOR PROBLEM ##
    #######################################
    b = 10
    h = 1
    LOSS(z, y) = b*max.(y-z, 0) + h*max.(z-y, 0)

    # CONTEXT OF INTEREST
    xbar = [10, 5]

    z_t = decision(xbar[1], xbar[2], b, h)
    c_t(z) = cost_model(z, xbar[1], xbar[2], b, h)

    #####################################
    ## Find an appropriate NW smoother ##
    #####################################
    Sn = Weighter(naive_smoother, (x_1, x_2)->norm(x_1-x_2), 1)

    ############################
    ## NW nominal formulation ##
    ############################
    c_n, z_n, s_n = news_vendor_nw_nominal(Y_tr, X_tr, Sn, xbar, b, h)

    L = [LOSS(z_n, Y_tr[i, :]) for i in 1:size(Y_tr, 1)]
    c_p, PR, s, s_p = nadaraya_watson_bootstrap_primal(L, X_tr, Sn, xbar, 0)
    c_d, s_d = nadaraya_watson_bootstrap_dual(L, X_tr, Sn, xbar, 0)

    push!(R, [run, n, Sn.smoother.name, Sn.bandwidth, 0, 1, c_t(z_n), c_n, c_p, c_d, s_n, s_p, s_d])

    CSV.write("Results/R_na_"*string(run)*".csv", R)

    println("R:\n", R[:, [:b, :c_t, :c, :c_d, :c_p]])

end
