using DataFrames
using CSV

using BootstrapRobustAnalytics
using NWNewsVendor

# COST FUNCTION NEWS VENDOR PROBLEM
b = 10
h = 1
LOSS(z, y) = b*max.(y-z, 0) + h*max.(z-y, 0)

function empirical_disappointment(z, c, LOSS, X, Y, xbar, Sn; n=0, m=1000, verbose=true)
    if n==0
        n = size(Y, 1)
    end

    if verbose
        println("# Start Empirical Disappointment")
        println("********************************")
    end
    
    Z = [LOSS(z, Y[i, :])[1] for i in 1:size(Y, 1)]
    c_bs = nadaraya_watson_bs(Z, X, xbar, Sn, n=n, m=m, verbose=verbose)

    if verbose
        println("************************************")
        println("*** End Empirical Disappointment ***")
    end

    return length(find(s->s > c, c_bs))/length(c_bs)
end

# CONTEXT OF INTEREST
xbar = [15, 5]'

# NUMBER OF DATA POINTS INCLUDED IN TRAINING SET
if length(ARGS)==0
    ns = [100]
    rs = [2e-3]
    mlt = [1]
else
    ns = 30:30:900
    ns = [ns[parse(Int, ARGS[1])]]
    rs = [2e-3,8e-3]
    mlt = 1:20
end

R = DataFrame(n=[], S=[], h=[], r=[], c=[], c_out=[], b_in=[], b_gar=[], status=[])

for n in ns, _ in mlt

    # LOAD DATA
    X_data = Array(CSV.read("X_nt.csv"))
    Y_data = Array(CSV.read("Y_nt.csv"))

    # Training data
    perm = shuffle(1:size(X_data, 1))
    X_data = X_data[perm, :]
    Y_data = Y_data[perm, :]
    X_tr = X_data[1:n, :]
    Y_tr = Y_data[1:n, :]
    X_val = X_data[n+1:end, :]
    Y_val = Y_data[n+1:end, :]

    # Find an appropriate nw smoother
    Sn = nadaraya_watson_v(Y_tr, X_tr, verbose=true)

    # NW nominal formulation
    (c_n, z_n, s_n) = news_vendor_nw_nominal(Y_tr, X_tr, Sn, xbar', b, h, verbose=true)

    # IN-SAMPLE
    b_in = empirical_disappointment(z_n, c_n, LOSS, X_tr, Y_tr, xbar, Sn, m=1000)
    # OUT-SAMPLE
    c_out = nadaraya_watson_learner([LOSS(z_n, Y_val[i, :])[1] for i in 1:size(Y_val, 1)], X_val, xbar, Sn, verbose=true)[1]
    push!(R, [n, Sn.smoother.name, Sn.bandwidth, 0, c_n, c_out, b_in, 1, s_n])

    for r in rs

        (c_r, z_r, s_r) = news_vendor_nw_bootstrap( Y_tr, X_tr, Sn, xbar', b, h, r, verbose=true)

        # IN-SAMPLE
        b_in = empirical_disappointment(z_r, c_r, LOSS, X_tr, Y_tr, xbar, Sn, m=round(Int, 200/exp(-r*n)))
        # OUT-SAMPLE
        c_out = nadaraya_watson_learner([LOSS(z_r, Y_val[i, :])[1] for i in 1:size(Y_val, 1)], X_val, xbar, Sn, verbose=true)[1]
        b_gar = exp(-n*r)
        push!(R, [n, Sn.smoother.name, Sn.bandwidth, r, c_r, c_out, b_in, b_gar, s_r])

    end

    # SAVE
    if length(ARGS)==0
        CSV.write("Results/NW-NewsVendor.csv", R)
    else
        CSV.write("Results/NW-NewsVendor-"*ARGS[1]*".csv", R)
    end

end
