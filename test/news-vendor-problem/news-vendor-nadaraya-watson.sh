#!/bin/bash
#SBATCH -a 1-200
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=8000
#SBATCH --time=0-06:00:00
#SBATCH -p sched_mit_sloan_batch

srun julia news-vendor-nadaraya-watson.jl $SLURM_ARRAY_TASK_ID
