using Gadfly
using DataFrames
using CSV

R_na = reduce(vcat, map(CSV.read, filter(x->contains(x, "_na_"), readdir())))

############
## FILTER ##
############

R_na_f = R_na[find(s->s!=1, R_na[:j]), :]

R_na_f = R_na_f[find(s->abs(s)<=1, R_na_f[:c] -R_na_f[:c_p]), :]
#R_na_f = R_na_f[find(s->abs(s)<=1, R_na_f[:c] -R_na_f[:c_d]), :]

R_na_fa = by(R_na_f, [:b]) do df
    DataFrame(c_t=mean(df[:c_t]), c_t_s2 = std(df[:c_t])/sqrt(size(df[:c_t], 1)), n = size(df[:c_t], 1))
end

R_na_faa=R_na_fa[find(s->abs(s-1)<0.1, R_na_fa[:b]), :]

##########
## PLOT ##
##########

CSV.write("R_na.csv", R_na_faa)

# R_na_fa = groupby(R_na_fa)
