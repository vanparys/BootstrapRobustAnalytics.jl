using Gadfly
using DataFrames
using CSV

R_nw = reduce(vcat, map(CSV.read, filter(x->contains(x, "_nw_"), readdir())))

############
## FILTER ##
############

R_nw_f = R_nw[find(s->s!=1, R_nw[:j]), :]

R_nw_f = R_nw_f[find(s->abs(s)<=1, R_nw_f[:c] -R_nw_f[:c_p]), :]
R_nw_f = R_nw_f[find(s->abs(s)<=1, R_nw_f[:c] -R_nw_f[:c_d]), :]

R_nw_fa = by(R_nw_f, [:d, :b]) do df
    DataFrame(c_t=mean(df[:c_t]), c_t_s2 = std(df[:c_t])/sqrt(size(df[:c_t], 1)), n = size(df[:c_t], 1))
end

R_nw_far = R_nw_fa[find(s->abs(s-1)>=0.1, R_nw_fa[:b]), :]
# R_nw_far = R_nw_fa
R_nw_far_a = by(R_nw_far, :d) do df
    index = indmin(df[:c_t])
    DataFrame(c_t=df[:c_t][index], c_t_min=df[:c_t][index]-1.96*df[:c_t_s2][index], c_t_max=df[:c_t][index]+1.96*df[:c_t_s2][index],
c_t_minus=1.96*df[:c_t_s2][index], c_t_plus=1.96*df[:c_t_s2][index],
              T="NW-R")
end
R_nw_fan = R_nw_fa[find(s->abs(s-1)<=0.1, R_nw_fa[:b]), :]
R_nw_fan_a = by(R_nw_fan, :d) do df
    index = indmin(df[:c_t])
    DataFrame(c_t=df[:c_t][index], c_t_min=df[:c_t][index]-1.96*df[:c_t_s2][index], c_t_max=df[:c_t][index]+1.96*df[:c_t_s2][index],
c_t_minus=1.96*df[:c_t_s2][index], c_t_plus=1.96*df[:c_t_s2][index],
              T="NW-N")
end

R_nw_fa_a = vcat(R_nw_far_a, R_nw_fan_a)

R_nw_far_a = R_nw_far_a[sortperm(R_nw_far_a[:d]), :]
R_nw_fan_a = R_nw_fan_a[sortperm(R_nw_fan_a[:d]), :]

CSV.write("R_nwr.csv", R_nw_far_a)
CSV.write("R_nwn.csv", R_nw_fan_a)


##########
## PLOT ##
##########

R_na = CSV.read("R_na.csv")

plot(R_nw_fa_a, x=:d, y=:c_t, y_min=:c_t_min, y_max=:c_t_max, color=:T, Geom.line, Geom.point, Geom.errorbar, Geom.hline(color=["red","black"], size=[1mm,1mm]), yintercept=[R_na[:c_t][1], 10.675]) |> PDF("NW.pdf")

# R_nw_fa = groupby(R_nw_fa)
