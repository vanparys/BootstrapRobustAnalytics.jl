using Random
Random.seed!(1232)
using Distributions
using PDMats
using DataFrames
using CSV

# Portfolio problem
n = 50000

# Covariates
TEMPERATURE = Normal(20, 7)
DAY = Categorical(ones(7)/7)
X_nt = DataFrame(TEMPERATURE=rand(TEMPERATURE, n), DAY=rand(DAY, n))

μ_nt = 100 + (X_nt[:TEMPERATURE]-20)*2
μ_nt[find(s->s>=6, X_nt[:DAY])] = μ_nt[find(s->s>=6, X_nt[:DAY])] + 20

Y_nt = μ_nt + 4*randn(n)
Y_nt = DataFrame(DEMAND=Y_nt[:, 1])

# Write to file
CSV.write("X_nt.csv", X_nt)
CSV.write("Y_nt.csv", Y_nt)





function PROFIT(z, T, D)

    μ = 100+(T-20)+20*(D==6||D==7)
    v = 16

end

